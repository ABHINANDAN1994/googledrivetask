//
//  QuickAccessData.swift
//  GoogleDriveTask
//
//  Created by Sierra 4 on 23/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import Foundation

import UIKit

class dataForQuickAcess
{
    var image : UIImage
    var name : String
    var status : String
    var type : UIImage
    
    init(image : UIImage , name : String , status : String , type : UIImage)
    {
        self.image = image
        self.name = name
        self.status = status
        self.type = type
    }
}
