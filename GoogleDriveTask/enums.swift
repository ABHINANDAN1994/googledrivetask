//
//  enums.swift
//  GoogleDriveTask
//
//  Created by Sierra 4 on 20/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import Foundation


import Foundation

enum identifier: String
{
    case tableCellFirst = "QuickAccessTVCellId"
    case tableCellSecond = "FoldersTVCellId"

    
    case collectionCellFirst = "QuickAccessCVCellId"
    case collectionCellSecond = "FoldersCVCellId"

    case sidePanelTableCell = "TableCellSlideridentifier"
    case sidePanelSearchTableCell = "TableCellSearchSlideridentifier"

    //case SliderIdentifierSearch = "SliderIdentifierSearch"
    case SliderIdentifierLeft = "SliderIdentifierLeft"
}
//
//
enum identifierClasses: String
{
    case TableCellFirst = "QuickAccessTableViewCell"
    case TableCellSecond = "FoldersTableViewCell"

    case CollectionCellFirst = "QuickAccessCollectionViewCell"
    case CollectionCellSecond = "FoldersCollectionViewCell"

    case sectionHeader = "sectionHeader"
}
//
//enum constants: String
//{
//    case areaId = "areaId"
//    case countryId = "countryId"
//    case alamofireRequest = "https://appbean.clikat.com/get_all_category"
//    case error = "errorOccured"
//}
//
enum labels: String
{
    case fileTypes = "File Types"
    case dateModified = "Date Modified"
    case quickAccess = "Quick Access"
    case files = "Files"
    case name = "NAME"
    case font = "Avenir next"
 }

enum sliderOptions: String
{
    case myDrive = "My Drive"
    case sharedWithMe = "Shared With Me"
    case googlePhotos = "Google Photos"
    case recent = "Recent"
    case starred = "Starred"
    case Offline = "Offline"
    case Bin = "Bin"
    
    case notifications = "notifications"
    case settings = "settings"
    case helpAndFeedback = "Help And Feedback"
    case storage = "Storage"
    
    
    
    case pdfs = "PDFs"
    case textDocuments = "Text Documents"
    case spreadsheets = "Spreadsheets"
    case presentations = "Presentations"
    case photosAndImages = "Photos & images"
    case videos = "Videos"
    case folders = "Folders"
    case audio = "Audio"
    
    case today = "Today"
    case yesterday = "Yesterday"
    case past7days = "Past 7 days"
    case past30days = "Past 30 days"
    case past90days = "Past 90 days"
    
    
    
    case sortby = "sort by.."
    case select = "select.."
    case selectAll = "selectAll.."

}





