


import Foundation
import Firebase


class UserClass {
    var usersname = ""
    var email = ""
    var password = ""
    var photoUrl = ""
    var uid = ""
    
    init(withSnapshot: FIRDataSnapshot) {
        let dict = withSnapshot.value as! [String:AnyObject]
        
        uid = withSnapshot.key
        usersname = dict["usersname"] as! String
        email = dict["email"] as! String
        password = dict["password"] as! String
        photoUrl = dict["photoUrl"] as! String
    }

}
