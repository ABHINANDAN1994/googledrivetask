//
//  DataForSlider.swift
//  GoogleDriveTask
//
//  Created by Sierra 4 on 20/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import Foundation
import UIKit

class dataForSlider
{
    var image : UIImage
    var name : String
    
    init(image : UIImage , name : String)
    {
        self.image = image
        self.name = name
    }
}
