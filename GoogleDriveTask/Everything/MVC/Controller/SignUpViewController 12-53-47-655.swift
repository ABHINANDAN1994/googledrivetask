//
//  SignUpViewController.swift
//  Instagram
//
//  Created by Sierra 4 on 14/02/17.
//  Copyright © 2017 codeBrew. All rights reserved.
//

import UIKit
import Firebase

class SignUpViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet weak var txtName: UITextField!
    
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var txtPassword: UITextField!
    
    
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    @IBOutlet weak var imgProfile: UIImageView!
    
    @IBOutlet weak var btnNext: UIButton!
    
    let picker = UIImagePickerController()
    var userStorage: FIRStorageReference!
    var ref: FIRDatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.delegate = self
        
        let storage = FIRStorage.storage().reference(forURL: "gs://project-6478218717827781171.appspot.com")
        ref = FIRDatabase.database().reference()
        userStorage = storage.child("users")
        
        
        btnNext.layer.cornerRadius=20
        btnNext.layer.masksToBounds=true
        btnNext.layer.borderColor=UIColor.init(red: 198, green: 16, blue: 53, alpha: 0.5).cgColor
        btnNext.layer.borderWidth=2
        
        
    }
    
    @IBAction func selectImagePressed(_ sender: Any) {
        
        picker.allowsEditing = true
        picker.sourceType = .photoLibrary
        
        present(picker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            
            self.imgProfile.image = image
            btnNext.isHidden = false
        }
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func nextPressed(_ sender: Any) {
        
        guard txtName.text != "",txtEmail.text != "",txtPassword.text != "",txtConfirmPassword.text != ""  else {
            return
        }
        
        if txtPassword.text == txtConfirmPassword.text {
            
            FIRAuth.auth()?.createUser(withEmail: txtEmail.text!, password: txtPassword.text!, completion: { (user, error) in
                
                if let error = error {
                    print(error.localizedDescription)
                }
                if let user = user {
                    
                    let changeRequest = FIRAuth.auth()!.currentUser!.profileChangeRequest()
                    changeRequest.displayName = self.txtName.text!
                    changeRequest.commitChanges(completion: nil)
                    
                    let imageRef = self.userStorage.child("\(user.uid).jpg")
                    let data = UIImageJPEGRepresentation(self.imgProfile.image!, 0.5)
                    let uploadTask = imageRef.put(data!, metadata: nil, completion: { (metadata, err) in
                        if err != nil
                        {
                            print(err!.localizedDescription)
                        }
                        
                        imageRef.downloadURL(completion: { (url, er) in
                            if er != nil
                            {
                                print(er!.localizedDescription)
                            }
                            
                            if let url = url {
                                
                                
                                let userInfo:[String : Any] = ["uid": user.uid, "full name":self.txtName.text!,"urlToImage" : url.absoluteString]
                                
                                self.ref.child("users").child(user.uid).setValue(userInfo)
                                
                                let vc = UIStoryboard(name: "main", bundle: nil).instantiateViewController(withIdentifier: "usersVC")
                                
                                self.present(vc, animated: true, completion: nil)
                                
                            }
                            
                            
                            
                        })
                    })
                    
                    uploadTask.resume()
                }
                
            })
            
            
        } else {
            print("Passwords Does not Match")
        }
        
        
        
        
    }
    
    
    
    
}
