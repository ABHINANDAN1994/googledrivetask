//
//  RightPopViewController.swift
//  GoogleDriveTask
//
//  Created by Sierra 4 on 22/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit

class RightPopViewController: UIViewController {

    var rightPopArray :[String] = ["Sort by",
                            "Select...","Select all..","Details"]

    override func viewDidLoad()
    {
        super.viewDidLoad()
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(changeGesture))
        gestureRecognizer.numberOfTapsRequired = 1
        view.addGestureRecognizer(gestureRecognizer)
    }
    
    func changeGesture()
    {
        let viewMenu : UIView = view
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
        var frameMenu : CGRect = viewMenu.frame
        frameMenu.origin.x = 0
        viewMenu.frame = frameMenu
        viewMenu.layoutIfNeeded()
        viewMenu.backgroundColor = UIColor.clear
            }, completion: { (finished) -> Void in
                viewMenu.removeFromSuperview()
            })
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        let viewMenu : UIView = view
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
                var frameMenu : CGRect = viewMenu.frame
                frameMenu.origin.x = 0
                viewMenu.frame = frameMenu
                viewMenu.layoutIfNeeded()
                viewMenu.backgroundColor = UIColor.clear
            }, completion: { (finished) -> Void in
                viewMenu.removeFromSuperview()
            })
            
    }
}
    
extension RightPopViewController: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return rightPopArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        guard let cell:RightPopTableViewCell = tableView.dequeueReusableCell(withIdentifier: "RightPopidentifier", for: indexPath) as? RightPopTableViewCell
        else
        {
            return RightPopTableViewCell()
        }
        cell.lblRightPop.text = rightPopArray[indexPath.row]
        return cell
        
    }
        
}
    

