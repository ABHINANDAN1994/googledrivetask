//
//  SliderViewController.swift
//  GoogleDriveTask
//
//  Created by Sierra 4 on 20/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit
import Firebase

class SliderViewController: UIViewController
{
    var headerSetting : sectionHeader?
    
    
    
    @IBOutlet weak var btnAccountsManage: UIButton!
    @IBOutlet weak var userName: UILabel!
    
    @IBOutlet weak var userEmail: UILabel!
    
    //@IBOutlet weak var viewForSlider: UIView!
    //@IBOutlet weak var LocationView: UIView!
    //@IBOutlet weak var UserProfilePic: UIImageView!
    
    @IBOutlet weak var profileImage: UIImageView!
    var menuOptionsLeftSlider =
        [
            [
                dataForSlider(image:#imageLiteral(resourceName: "ic_star_rate_18pt"),name:sliderOptions.myDrive.rawValue),
                dataForSlider(image:#imageLiteral(resourceName: "ic_supervisor_account"),name:sliderOptions.sharedWithMe.rawValue),
                dataForSlider(image:#imageLiteral(resourceName: "ic_schedule_18pt"),name:sliderOptions.googlePhotos.rawValue),
                dataForSlider(image:#imageLiteral(resourceName: "ic_schedule_18pt"),name:sliderOptions.recent.rawValue),
                dataForSlider(image:#imageLiteral(resourceName: "ic_star_rate_18pt"),name:sliderOptions.starred.rawValue),
                dataForSlider(image:#imageLiteral(resourceName: "ic_check_box"),name:sliderOptions.Offline.rawValue),
                dataForSlider(image:#imageLiteral(resourceName: "ic_donut_large_18pt"),name:sliderOptions.Bin.rawValue)
            ]
        ,
            [
                dataForSlider(image:#imageLiteral(resourceName: "ic_notifications_18pt"),name:sliderOptions.notifications.rawValue),
                dataForSlider(image:#imageLiteral(resourceName: "ic_settings_18pt"),name:sliderOptions.settings.rawValue),
                dataForSlider(image:#imageLiteral(resourceName: "ic_help_18pt"),name:sliderOptions.helpAndFeedback.rawValue),
                dataForSlider(image:#imageLiteral(resourceName: "ic_settings_18pt"),name:sliderOptions.storage.rawValue),
            ]
        ]
    
    
//    var menuOptionsRightSlider =
//        [
//            [
//                dataForSlider(image:#imageLiteral(resourceName: "ic_home"),name:sliderOptions.sortby.rawValue),
//                dataForSlider(image:#imageLiteral(resourceName: "ic_home"),name:sliderOptions.select.rawValue),
//                dataForSlider(image:#imageLiteral(resourceName: "ic_shopping_cart"),name:sliderOptions.selectAll.rawValue),
//                
//            ]
//        ]
//
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        profileImage.layer.cornerRadius = profileImage.frame.height/2
//        UserProfilePic.layer.borderWidth = 1
//        UserProfilePic.layer.borderColor = UIColor.white.cgColor
//        LocationView.layer.cornerRadius = 5
//        LocationView.layer.masksToBounds = true
//        
        
//        if !UIAccessibilityIsReduceTransparencyEnabled()
//        {
//            self.viewForSlider.backgroundColor = UIColor.clear
//            
//            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
//            let blurEffectView = UIVisualEffectView(effect: blurEffect)
//            blurEffectView.frame = self.viewForSlider.bounds
//            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//            
//            self.view.addSubview(blurEffectView) //if you have more UIViews, use an insertSubview API to place it where needed
//            self.view.sendSubview(toBack: blurEffectView)
//            
//        }
//        else
//        {
//            //self.viewSideMenu.backgroundColor = UIColor.black
//        }
//        
        
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        Back.backPanel(obj: self)
    }
    
       
}
extension SliderViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        
        let nib = UINib(nibName: identifierClasses.sectionHeader.rawValue, bundle: nil)
        let headerSetting = nib.instantiate(withOwner: nil, options: nil)[0] as? sectionHeader
        
//        headerSetting?.sliderLabelOutlet.textColor = UIColor.white
//        headerSetting?.sliderLabelOutlet.font = UIFont(name: (headerSetting?.sliderLabelOutlet.font.fontName)!, size:12.0 )
//        headerSetting?.sliderLabelOutlet.text = labels.MyAccount.rawValue
//        headerSetting?.sliderButtonOutlet.isHidden = true
        
        return headerSetting
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 0
    }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return menuOptionsLeftSlider[section].count
    }
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return menuOptionsLeftSlider.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        guard let cell : SliderTableViewCell = tableView.dequeueReusableCell(withIdentifier: identifier.sidePanelTableCell.rawValue, for: indexPath) as? SliderTableViewCell else{ return SliderTableViewCell()}
        
        cell.DataForSlider = menuOptionsLeftSlider[indexPath.section][indexPath.row]
        return cell
    }
}
