//
//  sliderSearchViewController.swift
//  GoogleDriveTask
//
//  Created by Sierra 4 on 20/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit

class sliderSearchViewController: UIViewController
{
    @IBOutlet weak var backButtonSearchPage: UIButton!
    
    var menuOptionsSearchSlider =
        [
            [
                dataForSlider(image:#imageLiteral(resourceName: "iamge3"),name:sliderOptions.pdfs.rawValue),
                dataForSlider(image:#imageLiteral(resourceName: "iamge3"),name:sliderOptions.textDocuments.rawValue),
                dataForSlider(image:#imageLiteral(resourceName: "iamge3"),name:sliderOptions.spreadsheets.rawValue),
                dataForSlider(image:#imageLiteral(resourceName: "iamge3"),name:sliderOptions.presentations.rawValue),
                dataForSlider(image:#imageLiteral(resourceName: "iamge3"),name:sliderOptions.photosAndImages.rawValue),
                dataForSlider(image:#imageLiteral(resourceName: "iamge3"),name:sliderOptions.videos.rawValue),
                dataForSlider(image:#imageLiteral(resourceName: "iamge3"),name:sliderOptions.folders.rawValue),
                dataForSlider(image:#imageLiteral(resourceName: "iamge3"),name:sliderOptions.audio.rawValue)
            ]
            ,
            [
                dataForSlider(image:#imageLiteral(resourceName: "iamge3"),name:sliderOptions.today.rawValue),
                dataForSlider(image:#imageLiteral(resourceName: "iamge3"),name:sliderOptions.yesterday.rawValue),
                dataForSlider(image:#imageLiteral(resourceName: "iamge3"),name:sliderOptions.past7days.rawValue),
                dataForSlider(image:#imageLiteral(resourceName: "iamge3"),name:sliderOptions.past30days.rawValue),
                dataForSlider(image:#imageLiteral(resourceName: "iamge3"),name:sliderOptions.past90days.rawValue)
            ]
        ]
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    @IBAction func backButtonSearchPage(_ sender: Any)
    {
        _ = navigationController?.popViewController(animated: true)
    }
 }

    extension sliderSearchViewController: UITableViewDelegate,UITableViewDataSource{
        
        func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
        {
            
            let nib = UINib(nibName: identifierClasses.sectionHeader.rawValue, bundle: nil)
            let headerSetting = nib.instantiate(withOwner: nil, options: nil)[0] as? sectionHeader
            
                    headerSetting?.sliderLabelOutlet.textColor = UIColor.lightGray
                    headerSetting?.sliderLabelOutlet.font = UIFont(name: (headerSetting?.sliderLabelOutlet.font.fontName)!, size:12.0 )
                    if(section == 0)
                    {
                        headerSetting?.sliderLabelOutlet.text = labels.fileTypes.rawValue
                    }
                    else
                    {
                        headerSetting?.sliderLabelOutlet.text = labels.dateModified.rawValue
                    }
                   // headerSetting?.sliderButtonOutlet.isHidden = true
            
            return headerSetting
            
        }
        func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
        {
            return 40
        }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
        {
            return menuOptionsSearchSlider[section].count
        }
        func numberOfSections(in tableView: UITableView) -> Int
        {
            return menuOptionsSearchSlider.count
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
        {
            guard let cell : slideSearchTableViewCell = tableView.dequeueReusableCell(withIdentifier: identifier.sidePanelSearchTableCell.rawValue, for: indexPath) as? slideSearchTableViewCell else{ return SliderTableViewCell()}
            
            cell.DataForSlider = menuOptionsSearchSlider[indexPath.section][indexPath.row]
            return cell
        }
    }
