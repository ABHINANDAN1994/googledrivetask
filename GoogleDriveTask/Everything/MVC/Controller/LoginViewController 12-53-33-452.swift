//
//  LoginViewController.swift
//  Instagram
//
//  Created by Sierra 4 on 14/02/17.
//  Copyright © 2017 codeBrew. All rights reserved.
//

import UIKit
import Firebase

class LoginViewController: UIViewController
{
    
    @IBOutlet weak var txtEmailLogin: UITextField!
    @IBOutlet weak var txtPasswordLogin: UITextField!
    
    @IBOutlet weak var viewForLoginSignup: UIView!
    @IBOutlet weak var btnLogin: UIButton!
    
    @IBOutlet weak var btnSignup: UIButton!
    

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        btnLogin.layer.cornerRadius = 5
        btnLogin.layer.masksToBounds = true
        btnLogin.layer.borderColor = UIColor.init(red: 198, green: 16, blue: 53, alpha: 0.5).cgColor
        btnLogin.layer.borderWidth = 2
        
        
        btnSignup.layer.cornerRadius = 5
        btnSignup.layer.masksToBounds = true
        btnSignup.layer.borderColor = UIColor.init(red: 198, green: 16, blue: 53, alpha: 0.5).cgColor
        btnSignup.layer.borderWidth = 2
        
        
        viewForLoginSignup.layer.borderWidth = 0.2
        viewForLoginSignup.layer.borderColor = UIColor.lightGray.cgColor
        
        
    }
    @IBAction func loginPressed(_ sender: Any)
    {
        guard txtEmailLogin.text != "",txtPasswordLogin.text != "" else
        {
            return
        }
        FIRAuth.auth()?.signIn(withEmail: txtEmailLogin.text!, password: txtPasswordLogin.text!, completion: { (user, error) in
            if let error = error
            {
                print(error.localizedDescription)
            }
            
            if user != nil
            {
            }
        })
        performSegue(withIdentifier: "LoginSuccessful", sender: self)
    }
    
}
