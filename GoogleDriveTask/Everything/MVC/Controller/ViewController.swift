//
//  ViewController.swift
//  GoogleDriveTask
//
//  Created by Sierra 4 on 20/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{
    @IBOutlet weak var TableView: UITableView!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var btnLeftSlider: UIButton!
    
    var headerSetting : sectionHeader?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        addButton.layer.cornerRadius =
        addButton.frame.height/2
        
        //TableView.estimatedRowHeight = 200
        //TableView.rowHeight = UITableViewAutomaticDimension
        TableView.delegate = self
        TableView.dataSource = self
        
        let nibTVQuickAccess = UINib(nibName: identifierClasses.TableCellFirst.rawValue, bundle: nil)
        TableView.register(nibTVQuickAccess, forCellReuseIdentifier: identifier.tableCellFirst.rawValue)
        
        
        let nibTVFolders = UINib(nibName: identifierClasses.TableCellSecond.rawValue, bundle: nil)
        TableView.register(nibTVFolders, forCellReuseIdentifier: identifier.tableCellSecond.rawValue)
    }
    
    
    //////////////////////LEFT SLIDER/////////////////////////////////////
    @IBAction func btnLeftSlider(_ sender: Any)
    {
        Appearance.ShowPanel(obj: self, identifier: identifier.SliderIdentifierLeft.rawValue)
    }
    
    
    ///////////////////////////RIGHT POP/////////////////////////////////
    @IBAction func BtnRightPop(_ sender: Any)
    {
        slideRight(para:self)
    }
        func slideRight(para:UIViewController)
    {
        let MenuSlide : RightPopViewController = para.storyboard!.instantiateViewController(withIdentifier: "RPidentifier") as! RightPopViewController
        para.view.addSubview(MenuSlide.view)
        para.addChildViewController(MenuSlide)
        MenuSlide.view.layoutIfNeeded()
        
        
        MenuSlide.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            MenuSlide.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
            
        }, completion:nil)
        
    }
    
    
    //////////////////////////////////////ADD BUTTON///////////////////////////
    @IBAction func addButton(_ sender: Any)
    {
        pushDownView(args: self)
    }
    func pushDownView(args:UIViewController)
    {
        let nib = UINib(nibName: "XibView", bundle: nil)
        let headerView = nib.instantiate(withOwner: nil, options: nil)[0] as? XibView
        headerView?.frame =  self.view.frame
        headerView?.bottomView.transform = CGAffineTransform.init(translationX: 0, y: +(headerView?.bottomView.frame.size.height)!)
        self.view.addSubview(headerView!)
        
        UIView.animate(withDuration: 0.5)
        {
            headerView?.bottomView.transform = CGAffineTransform.init(translationX: 0, y: 0)
        }
        
    }

    
}
/////////////////////////////TABLEVIEW/////////////////////////////////////////
extension ViewController : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let nib = UINib(nibName: identifierClasses.sectionHeader.rawValue, bundle: nil)
        let headerSetting = nib.instantiate(withOwner: nil, options: nil)[0] as? sectionHeader
        
        if(section == 0)
        {
            
            headerSetting?.sliderLabelOutlet.font = UIFont(name: (headerSetting?.sliderLabelOutlet.font.fontName)!, size: 14)
            headerSetting?.sliderLabelOutlet.text = labels.quickAccess.rawValue
            
            
            headerSetting?.sliderButtonOutlet.isHidden = true
            
            return headerSetting
        }
        else
        {
            headerSetting?.sliderLabelOutlet.font = UIFont(name:(headerSetting?.sliderLabelOutlet.font.fontName)!, size: 14)
            headerSetting?.sliderLabelOutlet.text = labels.files.rawValue
            
            headerSetting?.sliderButtonOutlet.isHidden = false
            headerSetting?.sliderButtonOutlet.titleLabel!.font =  UIFont(name: labels.font.rawValue, size: 14)
            headerSetting?.sliderButtonOutlet.setTitle(labels.name.rawValue, for: .normal)
            
            return headerSetting
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 30
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 150
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        switch(indexPath.section)
        {
            
        case 0:
            let QuickAccessTVCellId = tableView.dequeueReusableCell(withIdentifier: identifier.tableCellFirst.rawValue, for: indexPath) as! QuickAccessTableViewCell
            return QuickAccessTVCellId
            
        case 1:
            let FoldersTVCellId = tableView.dequeueReusableCell(withIdentifier: identifier.tableCellSecond.rawValue, for: indexPath) as! FoldersTableViewCell
            
            //FoldersTVCellId.ProductsCV.reloadData()
            return FoldersTVCellId
      
        default:
            let QuickAccessTVCellId = tableView.dequeueReusableCell(withIdentifier: identifier.tableCellFirst.rawValue, for: indexPath) as! QuickAccessTableViewCell
            return QuickAccessTVCellId
        }

    }
}



