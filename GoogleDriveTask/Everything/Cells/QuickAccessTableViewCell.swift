//
//  QuickAccessTableViewCell.swift
//  GoogleDriveTask
//
//  Created by Sierra 4 on 23/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit

class QuickAccessTableViewCell: UITableViewCell,  UICollectionViewDataSource , UICollectionViewDelegateFlowLayout  , UICollectionViewDelegate
{
    
    @IBOutlet weak var QuickAccessCV: UICollectionView!
    var quickAccessArray =
        [
            dataForQuickAcess(image : #imageLiteral(resourceName: "iamge3") , name : "Trainees 2017" , status : "Modified" , type : #imageLiteral(resourceName: "ic_check_box")),
            dataForQuickAcess(image : #imageLiteral(resourceName: "iamge3") , name : "Updated Technical" , status : "Updated" , type : #imageLiteral(resourceName: "ic_check_box")),
            dataForQuickAcess(image : #imageLiteral(resourceName: "iamge3") , name : "Data Passing" , status : "Modified" , type : #imageLiteral(resourceName: "ic_check_box")),
            dataForQuickAcess(image : #imageLiteral(resourceName: "iamge3") , name : "Updated Technical" , status : "Updated" , type : #imageLiteral(resourceName: "ic_check_box")),
            dataForQuickAcess(image : #imageLiteral(resourceName: "iamge3") , name : "Updated Technical" , status : "Updated" , type : #imageLiteral(resourceName: "ic_check_box")),
            dataForQuickAcess(image : #imageLiteral(resourceName: "iamge3") , name : "Updated Technical" , status : "Modified" , type : #imageLiteral(resourceName: "ic_check_box"))
            
        ]

    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        let nibCVQuickAccess = UINib(nibName: identifierClasses.CollectionCellFirst.rawValue, bundle: nil)
        QuickAccessCV.register(nibCVQuickAccess, forCellWithReuseIdentifier: identifier.collectionCellFirst.rawValue)
        
        QuickAccessCV.delegate =  self
        QuickAccessCV.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return quickAccessArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let QuickAccessCVCellId = collectionView.dequeueReusableCell(withReuseIdentifier: identifier.collectionCellFirst.rawValue, for: indexPath) as! QuickAccessCollectionViewCell
        
        QuickAccessCVCellId.DataForQuickAccess = quickAccessArray[indexPath.row]
     
        return QuickAccessCVCellId
    }
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width:(self.QuickAccessCV.frame.width/2)-8 ,height: (self.QuickAccessCV.frame.width/2)-8)
        }

}
















