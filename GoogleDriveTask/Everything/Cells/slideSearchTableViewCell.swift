//
//  slideSearchTableViewCell.swift
//  GoogleDriveTask
//
//  Created by Sierra 4 on 20/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit

class slideSearchTableViewCell: UITableViewCell {
    
    @IBOutlet weak var ButtonMenuItem: UIButton!
    @IBOutlet weak var lblMenuItem: UILabel!
    
    var DataForSlider: dataForSlider? {
        didSet{
            updateUI()
        }
    }
    
    fileprivate func updateUI()
    {
        ButtonMenuItem?.setImage(DataForSlider?.image, for: .normal)
        lblMenuItem.text = DataForSlider?.name
    }
    
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
}
