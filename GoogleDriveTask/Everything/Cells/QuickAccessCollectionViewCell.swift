//
//  QuickAccessCollectionViewCell.swift
//  GoogleDriveTask
//
//  Created by Sierra 4 on 23/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit

class QuickAccessCollectionViewCell: UICollectionViewCell
{

    @IBOutlet weak var quickAccessName: UILabel!
    @IBOutlet weak var quickAccessImage: UIImageView!
    @IBOutlet weak var quickAccessStatus: UILabel!
    @IBOutlet weak var quickAccessType: UIButton!
   
    var DataForQuickAccess: dataForQuickAcess? {
        didSet{
            updateUI()
        }
    }
    fileprivate func updateUI()
    {
        quickAccessImage.image = DataForQuickAccess?.image
        quickAccessName.text = DataForQuickAccess?.name
        quickAccessStatus.text = DataForQuickAccess?.status
        quickAccessType.setImage(DataForQuickAccess?.type, for: .normal)    }
    
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    
        
    }

}
